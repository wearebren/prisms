<project xmlns="http://maven.apache.org/POM/4.0.0"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>com.egfunds.prisms</groupId>
	<artifactId>Prisms</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<name>Prisms</name>
	<description>Automation for Prisms</description>

	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<selenium.version>3.13.0</selenium.version>
		<serenity.version>1.9.28</serenity.version>
		<junit.version>4.12</junit.version>
		<poi.version>3.17</poi.version>
		<hamcrest.version>1.3</hamcrest.version>
		<freemarker.version>2.3.28</freemarker.version>
	</properties>

	<dependencies>
		<!-- SELENIUM BASE -->
		<!-- https://mvnrepository.com/artifact/org.seleniumhq.selenium/selenium-java -->
		<dependency>
			<groupId>org.seleniumhq.selenium</groupId>
			<artifactId>selenium-java</artifactId>
			<version>${selenium.version}</version>
		</dependency>

		<!-- SERENITY FRAMEWORK -->
		<!-- https://mvnrepository.com/artifact/net.serenity-bdd/serenity-core -->
		<dependency>
			<groupId>net.serenity-bdd</groupId>
			<artifactId>serenity-core</artifactId>
			<version>${serenity.version}</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/net.serenity-bdd/serenity-junit -->
		<dependency>
			<groupId>net.serenity-bdd</groupId>
			<artifactId>serenity-junit</artifactId>
			<version>${serenity.version}</version>
		</dependency>

		<!-- TESTING WITH JUNIT -->
		<!-- https://mvnrepository.com/artifact/junit/junit -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- FOR EXCEL AND OTHER MICROSOFT FILES -->
		<!-- https://mvnrepository.com/artifact/org.apache.poi/poi -->
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi</artifactId>
			<version>${poi.version}</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.apache.poi/poi-ooxml -->
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
			<version>${poi.version}</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/org.apache.poi/poi-ooxml-schemas -->
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml-schemas</artifactId>
			<version>${poi.version}</version>
		</dependency>

		<!-- FOR VERIFICATION -->
		<!-- https://mvnrepository.com/artifact/org.hamcrest/hamcrest-library -->
		<dependency>
			<groupId>org.hamcrest</groupId>
			<artifactId>hamcrest-library</artifactId>
			<version>${hamcrest.version}</version>
			<scope>test</scope>
		</dependency>

		<!-- Others -->
		<!-- https://mvnrepository.com/artifact/org.freemarker/freemarker -->
		<dependency>
			<groupId>org.freemarker</groupId>
			<artifactId>freemarker</artifactId>
			<version>${freemarker.version}</version>
		</dependency>

	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>2.18.1</version>
				<configuration>
					<skip>true</skip>
				</configuration>
			</plugin>
			<plugin>
				<artifactId>maven-failsafe-plugin</artifactId>
				<version>2.18.1</version>
				<configuration>
					<includes>
						<include>**/*Test.java</include>
						<include>**/Test*.java</include>
						<include>**/When*.java</include>
						<include>**/*Story.java</include>
					</includes>
					<argLine>-Xmx1024m</argLine>
					<systemPropertyVariables>
						<webdriver.driver>${webdriver.driver}</webdriver.driver>
					</systemPropertyVariables>
				</configuration>
				<executions>
					<execution>
						<goals>
							<goal>integration-test</goal>
							<goal>verify</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>net.serenity-bdd.maven.plugins</groupId>
				<artifactId>serenity-maven-plugin</artifactId>
				<version>${serenity.version}</version>
				<executions>
					<execution>
						<id>serenity-reports</id>
						<phase>post-integration-test</phase>
						<goals>
							<goal>aggregate</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
		</plugins>
	</build>

	<profiles>
		<profile>
			<id>chrome</id>
			<build>
				<directory>${project.basedir}/target/site/serenity</directory>
				<plugins>
					<plugin>
						<artifactId>maven-failsafe-plugin</artifactId>
						<version>2.18</version>
						<configuration>
							<includes>
								<include>**/tests/*Test.java</include>
							</includes>
							<argLine>-Dfile.encoding=UTF-8</argLine>
							<systemProperties>
								<webdriver.driver>${webdriver.driver}</webdriver.driver>
								<webdriver.ie.driver>${ie.driver}</webdriver.ie.driver>
								<webdriver.gecko.driver>geckodriver.exe</webdriver.gecko.driver>
								<webdriver.chrome.driver>chromedriver.exe</webdriver.chrome.driver>
								<serenity.outputDirectory>${project.basedir}/target/site/serenity</serenity.outputDirectory>
								<file.encoding>${project.build.sourceEncoding}</file.encoding>
							</systemProperties>
						</configuration>
						<executions>
							<execution>
								<goals>
									<goal>integration-test</goal>
									<goal>verify</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>

		<profile>
			<id>iexplorer</id>
			<build>
				<directory>${project.basedir}/target/site/serenity</directory>
				<plugins>
					<plugin>
						<artifactId>maven-failsafe-plugin</artifactId>
						<version>2.18</version>
						<configuration>
							<includes>
								<include>**/tests/*Test.java</include>
							</includes>
							<argLine>-Dfile.encoding=UTF-8</argLine>
							<systemProperties>
								<webdriver.driver>${webdriver.driver}</webdriver.driver>
								<webdriver.ie.driver>${ie.driver}</webdriver.ie.driver>
								<webdriver.gecko.driver>geckodriver.exe</webdriver.gecko.driver>
								<webdriver.chrome.driver>chromedriver.exe</webdriver.chrome.driver>
								<serenity.outputDirectory>${project.basedir}/target/site/serenity</serenity.outputDirectory>
								<file.encoding>${project.build.sourceEncoding}</file.encoding>
							</systemProperties>
						</configuration>
						<executions>
							<execution>
								<goals>
									<goal>integration-test</goal>
									<goal>verify</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>

		<profile>
			<id>firefox</id>
			<build>
				<directory>${project.basedir}/target/site/serenity</directory>
				<plugins>
					<plugin>
						<artifactId>maven-failsafe-plugin</artifactId>
						<version>2.18</version>
						<configuration>
							<includes>
								<include>**/tests/*Test.java</include>
							</includes>
							<argLine>-Dfile.encoding=UTF-8</argLine>
							<systemProperties>
								<webdriver.driver>${webdriver.driver}</webdriver.driver>
								<webdriver.ie.driver>${ie.driver}</webdriver.ie.driver>
								<webdriver.gecko.driver>geckodriver.exe</webdriver.gecko.driver>
								<webdriver.chrome.driver>chromedriver.exe</webdriver.chrome.driver>
								<serenity.outputDirectory>${project.basedir}/target/site/serenity</serenity.outputDirectory>
								<file.encoding>${project.build.sourceEncoding}</file.encoding>
							</systemProperties>
						</configuration>
						<executions>
							<execution>
								<goals>
									<goal>integration-test</goal>
									<goal>verify</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
		
		<profile>
			<id>appium</id>
			<build>
				<directory>${project.basedir}/target/site/serenity</directory>
				<plugins>
					<plugin>
						<artifactId>maven-failsafe-plugin</artifactId>
						<version>2.18</version>
						<configuration>
							<includes>
								<include>**/tests/*Test.java</include>
							</includes>
							<argLine>-Dfile.encoding=UTF-8</argLine>
							<systemProperties>
								<webdriver.driver>${webdriver.driver}</webdriver.driver>
								<webdriver.ie.driver>${ie.driver}</webdriver.ie.driver>
								<webdriver.gecko.driver>geckodriver.exe</webdriver.gecko.driver>
								<webdriver.chrome.driver>chromedriver.exe</webdriver.chrome.driver>
								<serenity.outputDirectory>${project.basedir}/target/site/serenity</serenity.outputDirectory>
								<file.encoding>${project.build.sourceEncoding}</file.encoding>
							</systemProperties>
						</configuration>
						<executions>
							<execution>
								<goals>
									<goal>integration-test</goal>
									<goal>verify</goal>
								</goals>
							</execution>
						</executions>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

</project>
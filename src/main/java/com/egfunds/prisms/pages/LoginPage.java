package com.egfunds.prisms.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://prisms-uat.eg.com.au/Account/Login.aspx")
public class LoginPage extends PageObject {

	@FindBy(id = "ctl00_mainContent_UserName")
	private WebElementFacade usernameTxtbox;

	@FindBy(id = "ctl00_mainContent_Password")
	private WebElementFacade passwordTxtbox;

	@FindBy(name = "ctl00$mainContent$ctl01")
	private WebElementFacade loginButton;

	@FindBy(linkText = "ABOUT")
	private WebElementFacade aboutLink;

	@FindBy(linkText = "CONTACT")
	private WebElementFacade contactLink;
	
	@FindBy(id = "ctl00_mainContent_txtErrorMessage")
	private WebElementFacade errorMessage;

	public void inputUsername(String username) {

		typeInto(usernameTxtbox, username);
	}

	public void inputPassword(String password) {

		typeInto(passwordTxtbox, password);
	}

	public void clickLogin() {

		clickOn(loginButton);
	}

	public void clickAbout() {

		clickOn(aboutLink);
	}

	public void clickContact() {

		clickOn(contactLink);
	}
	
	public String getErrorMessage() {
		
		String errorMsg = errorMessage.getText().trim();
		
		return errorMsg;
	}


}

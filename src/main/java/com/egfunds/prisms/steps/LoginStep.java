package com.egfunds.prisms.steps;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import com.egfunds.prisms.pages.LoginPage;

import net.thucydides.core.annotations.Step;


public class LoginStep {
	
	LoginPage loginPage;
	
	@Step("Open and login to PRISMS")
	public void open_and_login_to_prisms(String username, String password) {
		
		open_prisms();
		set_username(username);
		set_password(password);
		click_login();
	}
	
	@Step("Open PRISMS")
	public void open_prisms() {
		
		loginPage.open();
		loginPage.getDriver().manage().window().maximize();
	}
	
	@Step("Input {0} in Username field")
	public void set_username(String username) {
		
		loginPage.inputUsername(username);
	}

	@Step("Input {0} in Password field")
	public void set_password(String password) {
		
		loginPage.inputPassword(password);
	}
	
	@Step("Clik Login button")
	public void click_login() {
		
		loginPage.clickLogin();
	}
	
	@Step("Click ABOUT link")
	public void click_about() {
		
		loginPage.clickAbout();
	}
	
	@Step("Click CONTACT link")
	public void click_contact() {
		
		loginPage.clickContact();
	}
	
	@Step("Verify error message")
	public void verify_error_message() {
		
		assertThat(loginPage.getErrorMessage(), equalTo("Invalid credentials."));
	}
}

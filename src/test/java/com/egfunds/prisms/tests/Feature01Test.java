package com.egfunds.prisms.tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import com.egfunds.prisms.steps.LoginStep;

import net.serenitybdd.junit.runners.SerenityRunner;
import net.thucydides.core.annotations.Managed;
import net.thucydides.core.annotations.Steps;
import net.thucydides.core.annotations.Title;

@RunWith(SerenityRunner.class)
public class Feature01Test {
	
	@Managed
    WebDriver driver;
	
	@Steps
	LoginStep loginStep;
	
	private String username;
	
	private String password;
	
	@BeforeClass
	public static void beforeClassTest() {
		
		System.out.println("Before Class");
	}
	
	@Before
	public void beforeTest() {
		
		System.out.println("Before");
	}
	
	@Test
	@Title("Failed Test Login")
	public void failed_test_login() {
		
		username = "blah";
		password= "blahblah";
		
		loginStep.open_and_login_to_prisms(username, password);
		loginStep.verify_error_message();
	}
	
	@After
	public void afterTest() {
		System.out.println("After");
	}
	
	@AfterClass
	public static void afterClassTest() {
		System.out.println("After Class");
	}
}
